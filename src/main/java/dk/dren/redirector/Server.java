package dk.dren.redirector;

import dk.dren.redirector.healthchecks.IsAliveCheck;
import dk.dren.redirector.injectors.InjectorBinder;
import dk.dren.redirector.resources.FrontPageResource;
import io.dropwizard.core.Application;
import io.dropwizard.core.setup.Environment;

/**
 * This is the main bootstrapping class of the application, it's what brings all the bits together and starts the application
 */
public class Server extends Application<ServerConfiguration> {

	/**
	 * This is the main entry point for the application, regardless of how it's started.
	 */
	public static void main(String[] args) throws Exception {
        try {
        	new Server().run(args);
        } catch (Throwable t) {
        	System.err.println("Failed while starting the application, giving up");
        	t.printStackTrace(System.err);
        	System.exit(254);
        }
    }

	/**
	 * Just set a human readable name of the application
	 */
	@Override
	public String getName() {
		return "Redirector";
	}

	@Override
	public void run(ServerConfiguration configuration, Environment environment) throws Exception {
		Redirects redirects = new Redirects(configuration.getSource());

		// Register injectors.
		environment.jersey().register(new InjectorBinder(configuration, redirects));

		// Register healthchecks, there really should be many more than just one.
		environment.healthChecks().register("Alive", new IsAliveCheck());

		// Register resources
		environment.jersey().register(FrontPageResource.class);
	}
}
