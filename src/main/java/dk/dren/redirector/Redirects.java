package dk.dren.redirector;

import jakarta.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Log
@RequiredArgsConstructor
public class Redirects {
    private static final Pattern TABLE = Pattern.compile("<table>(.+?)</table>", Pattern.DOTALL);
    private static final Pattern ROWS = Pattern.compile("<tr>(.+?)</tr>", Pattern.DOTALL);
    private static final Pattern COLS = Pattern.compile("<td>\\s*(.+?)\\s*</td>", Pattern.DOTALL);
    private static final Pattern HREF = Pattern.compile("href=\"(.+?)\"");
    private static final String UA = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/73.0.3683.75 Chrome/73.0.3683.75 Safari/537.36";

    private static final Object LOAD_LOCK = new Object();

    @Getter
    private final URI source;

    private long lastLoad = 0;

    private Map<URI, URI> sourceToTarget;

    private void refreshIfStale() throws IOException {
        synchronized (LOAD_LOCK) {
            long age = System.currentTimeMillis() - lastLoad;
            if (age > TimeUnit.MINUTES.toMillis(60)) {
                try {
                    refresh();
                } catch (Exception e) {
                    if (sourceToTarget != null) {
                        log.log(Level.SEVERE, "Failed to reload map from " + source, e);
                    } else {
                        throw new IOException("Failed to load map from " + source, e);
                    }
                }
            }
        }
    }

    public Map<URI, URI> refresh() throws IOException {
        synchronized (LOAD_LOCK) {
            long t0 = System.currentTimeMillis();

            try (CloseableHttpClient hc = HttpClients.createDefault()) {
                HttpGet get = new HttpGet(source);
                get.setHeader(HttpHeaders.USER_AGENT, UA); // Fucking unoeuro bs.

                try (CloseableHttpResponse response = hc.execute(get)) {
                    if (response.getStatusLine().getStatusCode() == 200) {
                        sourceToTarget = parse(IOUtils.toString(response.getEntity().getContent(), "UTF-8"));
                        lastLoad = System.currentTimeMillis();
                    } else {
                        throw new IOException("Got error from " + get + " " + response.getStatusLine());
                    }
                }
            }
            
            log.info("Loaded " + source + " in " + (System.currentTimeMillis()-t0) + " ms");
            return sourceToTarget;
        }
    }



    public static Map<URI, URI> parse(String html) {
        Map<URI, URI> res = new TreeMap<>();

        List<List<String>> table = parseFirstTable(html);

        for (List<String> row : table) {
            if (row.size() != 3) {
                continue;
            }

            URI src = findFirstHref(row.get(0));
            URI tgt = findFirstHref(row.get(1));

            if (src != null && tgt != null) {
                res.put(src, tgt);
            }
        }

        return res;
    }

    private static URI findFirstHref(String html) {
        Matcher matcher = HREF.matcher(html);
        if (matcher.find()) {
            return URI.create(matcher.group(1));
        } else {
            return null;
        }
    }

    private static List<List<String>> parseFirstTable(String html) {
        Matcher tableMatcher = TABLE.matcher(html);
        if (!tableMatcher.find()) {
            throw new IllegalArgumentException("Could not find the table in the html " + html);
        }

        List<List<String>> rows = new ArrayList<>();

        Matcher rowMatcher = ROWS.matcher(tableMatcher.group(1));
        while (rowMatcher.find()) {
            List<String> row = new ArrayList<>();
            Matcher colMatcher = COLS.matcher(rowMatcher.group(1));
            while (colMatcher.find()) {
                row.add(colMatcher.group(1));
            }
            rows.add(row);
        }
        return rows;
    }


    public URI getRedirect(HttpServletRequest request) {
        String server = request.getServerName();
        URI src = URI.create("http://" + server);

        try {
            refreshIfStale();
        } catch (IOException e) {
            log.log(Level.SEVERE, "Failed to refresh, but there's no map to resolve " + server, e);
            return null;
        }

        URI tgt = sourceToTarget.get(src);
        if (tgt != null) {
            if (request.getPathInfo().equals("/")) {
                return tgt;
            } else {
                return URI.create(tgt + request.getPathInfo());
            }
        } else {
            return null;
        }
    }
}
