package dk.dren.redirector;

import io.dropwizard.core.Configuration;
import lombok.Data;

import java.net.URI;

/**
 * Add fields for the configuration in this class,
 * Dropwizard will take care of reading the server,yaml file.
 * Don't bother creating getters and setters, @Data from Lombok creates getters and setters automagically.
 */
@Data
public class ServerConfiguration extends Configuration {
    private URI source;
}