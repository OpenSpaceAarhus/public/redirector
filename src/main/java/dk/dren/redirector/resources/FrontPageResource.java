package dk.dren.redirector.resources;

import com.codahale.metrics.annotation.Timed;
import dk.dren.redirector.Redirects;
import dk.dren.redirector.ServerConfiguration;
import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import lombok.extern.java.Log;

import java.net.URI;
import java.util.Map;
import java.util.logging.Level;

/**
 * Serves up the front page, though without playing nice with caches, because there's no ETag or conditional get support.
 */
@Path("/{hest: .*}")
@Log
public class FrontPageResource {
    @Inject
    ServerConfiguration config;

    @Inject
    Redirects redirects;

    @Context
    HttpServletRequest request;

    @POST
    @Timed
    @Produces("text/plain")
    public String getReload() {
        StringBuffer sb = new StringBuffer();
        try {
            Map<URI, URI> map = redirects.refresh();
            sb.append("Loaded redirects from ").append(redirects.getSource()).append("\n");
            for (Map.Entry<URI, URI> kv : map.entrySet()) {
                sb.append(kv.getKey()).append(" -> ").append(kv.getValue()).append("\n");
            }
        } catch (Exception e) {
            sb.append("Failed to load ").append(redirects.getSource()).append("\n");
            log.log(Level.SEVERE, "Failed to reload from " + redirects.getSource(), e);
        }
        return sb.toString();
    }

    @GET
    @Timed
    @Produces("text/plain")
    public Response get(@QueryParam("reload")String reload) {
        if (reload != null) {
            return Response.ok(getReload()).build();
        }
        URI target = redirects.getRedirect(request);
        if (target == null)  {
            return Response.seeOther(config.getSource()).build();
        } else {
            return Response.seeOther(target).build();
        }
    }
}