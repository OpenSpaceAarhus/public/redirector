package dk.dren.redirector.healthchecks;

import com.codahale.metrics.health.HealthCheck;

/**
 * This is just an example of a healthcheck, I doubt it's actually useful.
 */
public class IsAliveCheck extends HealthCheck {
    @Override
    protected Result check() throws Exception {

        return Result.healthy("OK");
    }
}
