package dk.dren.redirector;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

public class RedirectsTest {

    @Test
    public void parse() throws IOException {
        String html = IOUtils.toString(RedirectsTest.class.getResource("/hest.html"));
        Map<URI, URI> map = Redirects.parse(html);
        Assert.assertFalse(map.isEmpty());
    }
}