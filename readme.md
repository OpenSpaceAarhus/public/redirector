```
______         _ _               _             
| ___ \       | (_)             | |            
| |_/ /___  __| |_ _ __ ___  ___| |_ ___  _ __ 
|    // _ \/ _` | | '__/ _ \/ __| __/ _ \| '__|
| |\ |  __| (_| | | | |  __| (__| || (_) | |   
\_| \_\___|\__,_|_|_|  \___|\___|\__\___/|_|   
                                               
```

This is a very simple web server which does two things:
* Downloads a specific wiki page and parses it to find sub-domains and their targets.
* Sends users a 303 redirect when they request any of the sub-domains.


Building And Running
--------------------

* Build executable jar using: mvn package
* Run the app server using: java -jar target/redirector-0.0.1-SNAPSHOT.jar server server.yaml
* Open a browser and hit: http://localhost:8080/


Using IntelliJ IDEA
-------------------

* Import the project from the pom.xml
* The main class to start is dk.dren.recirector.Server
* This project uses Lombok to cut down on boilerplate, so be sure to install the appropriate lombok plugin for IDEA see: https://github.com/mplushnikov/lombok-intellij-plugin
